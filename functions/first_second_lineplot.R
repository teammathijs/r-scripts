first_second_lineplot <- function(dep_var,data){
summary<-summarySE(data = data, measurevar = dep_var, c('first.layer','second.layer','epochs'))
names(summary)[5] <- 'dep_var'
p1<-ggplot(summary[summary$epochs == 'epoch1',],aes(x= second.layer,y = dep_var , group = first.layer, color = first.layer)) +  geom_line() +   geom_errorbar(aes(ymin=dep_var-se, ymax=dep_var+se), width=.1)+
  ggtitle(' epoch 1') + theme_gray() + ylab(dep_var)

p3<-ggplot(summary[summary$epochs == 'epoch200',],aes(x= second.layer,y = dep_var , group = first.layer, color = first.layer)) +  geom_line() +   geom_errorbar(aes(ymin=dep_var-se, ymax=dep_var+se), width=.1)+
  ggtitle('epoch 200') + theme_gray() + ylab(dep_var)

p<-list(p1,p3)
return(p)
}
