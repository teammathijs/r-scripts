
plot_overview <- function(data, dep_var, my_comparisons) {


names(data)[1] <- 'epochs'

ggbar<-ggbarplot(data, x = "epochs", y = dep_var, fill= 'epochs',facet.by = 'task', add= 'mean') + 
  stat_compare_means(comparisons = my_comparisons ,method = 't.test', label = "p.signif", label.y = c(0.9,0.95,1)) 

return(ggbar)

}
