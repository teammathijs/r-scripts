layercomparison <-function(data){
  
  a <-aggregate(accuracy ~ epochs + layer , data , mean)
  maxi <-aggregate(accuracy ~ epochs , data,sd)
  ac<-cbind(a, maxi$accuracy,mini$accuracy)
  
  overallacc<-plot_ly(ac, x =~epochs, y =~accuracy, type = 'scatter', mode = 'lines',color  =~layer)%>%
    add_trace(y =~maxi$accuracy, type = 'scatter' , name = 'max',line = list(dash= 'dash'))%>%
    add_trace( y =~mini$accuracy ,type= 'scatter', name = 'min' , line = list(dash = 'dash'))%>%
    layout(title = 'accuracy between architecture over epochs',xaxis = list(title = 'epochs'),
           yaxis = list (title = 'accuracy'))
  
  
  ag <-aggregate(weberfraction ~ age + layer , data , mean)
  maxim <-aggregate(weberfraction ~ age +layer , data,max)
  minim <-aggregate(weberfraction ~ age + layer , data , min)
  w<-cbind(ag,maxim$weberfraction,minim$weberfraction)
  
  overallweb<-plot_ly(w, x =~age, y =~weberfraction, type = 'scatter', mode = 'lines',color  =~layer,showlegend = FALSE)%>%
    add_trace(y =~maxim$weberfraction, type = 'scatter' , name = 'max',line = list(dash= 'dash'),showlegend = FALSE)%>%
    add_trace( y =~minim$weberfraction ,type= 'scatter', name = 'min' , line = list(dash = 'dash'),showlegend = FALSE)%>%
    layout(title = 'weberfraction between architecture over epochs',
           xaxis = list(title = 'epochs'),
           yaxis = list (title = 'accuracy'))
  
  
  
  b <-subplot(overallacc,overallweb, shareX = TRUE,nrows = 2)%>%
    layout(title = 'performances between architectures over epochs')
  
  b<-b %>% layout(annotations = list(
    list(x = 0.2 , y = 1, text = "accuracy", showarrow = F, xref='paper', yref='paper'),
    list(x = 0.2 , y = 0.5, text = "weberfraction", showarrow = F, xref='paper', yref='paper')))
  
  return(b)
}
